class DrinksController < ApplicationController
  before_action :set_drink, only: [:show]

  # GET /drinks/uid
  # GET /drinks/uid.json
  def show
    puts params[:uid]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_drink
      @drink = Drink.where(:uid => params[:id]).limit(1).first
      puts @drink
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def drink_params
      params.require(:drink).permit(:uid)
    end
end
