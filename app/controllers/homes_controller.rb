class HomesController < ApplicationController

  def index
    @drinks = Drink.where("description IS NOT NULL AND image = true").sample(3)
  end

end
