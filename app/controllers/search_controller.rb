class SearchController < ApplicationController
  before_action :set_drink, only: [:show]

  def index
    @drinks = Drink.search params[:q]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_drink
      @drink = Drink.where(:uid => params[:id]).limit(1).first
      puts @drink
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def drink_params
      params.require(:drink).permit(:uid)
    end
end
