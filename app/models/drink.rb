class Drink < ActiveRecord::Base
  searchkick


  has_many :drink_ingredients, :class_name => 'DrinkIngredient', :foreign_key => :drink
  belongs_to :glass, :class_name => 'Glass', :foreign_key => :glass
end
