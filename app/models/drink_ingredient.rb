class DrinkIngredient < ActiveRecord::Base



    belongs_to :amount, :class_name => 'Amount', :foreign_key => :amount
    belongs_to :drink, :class_name => 'Drink', :foreign_key => :drink
    belongs_to :ingredient, :class_name => 'Ingredient', :foreign_key => :ingredient
end
