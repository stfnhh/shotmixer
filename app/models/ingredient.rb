class Ingredient < ActiveRecord::Base


    has_many :drink_ingredients, :class_name => 'DrinkIngredient'
    has_many :user_ingredients, :class_name => 'UserIngredient'
end
