class CreateDrinks < ActiveRecord::Migration
  def change
    create_table :drinks do |t|
      t.string :uid
      t.string :drink
      t.integer :glass
      t.text :instructions
      t.text :description
      t.boolean :image
    end
  end
end
