class CreateAmounts < ActiveRecord::Migration
  def change
    create_table :amounts do |t|
      t.string :amount
    end
  end
end
