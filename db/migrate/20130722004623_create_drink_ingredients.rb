class CreateDrinkIngredients < ActiveRecord::Migration
  def change
    create_table :drink_ingredients do |t|
      t.integer :drink
      t.integer :ingredient
      t.integer :amount
    end
  end
end
