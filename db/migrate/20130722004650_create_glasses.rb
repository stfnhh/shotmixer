class CreateGlasses < ActiveRecord::Migration
  def change
    create_table :glasses do |t|
      t.string :glass
    end
  end
end
