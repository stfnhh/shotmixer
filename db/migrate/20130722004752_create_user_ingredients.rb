class CreateUserIngredients < ActiveRecord::Migration
  def change
    create_table :user_ingredients do |t|
      t.integer :user
      t.integer :ingredient
    end
  end
end
