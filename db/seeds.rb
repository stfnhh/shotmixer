def q(f, m = true)
    sql = File.open("db/data/#{f}").read
    if m
        ActiveRecord::Base.connection.execute(sql)
    else
      sql.split("\n").each do |sql_statement|
        ActiveRecord::Base.connection.execute(sql_statement)
      end
    end
end

q("amounts.sql")
q("glasses.sql")
q("ingredients.sql")
q("drinks.sql", false)
q("drink_ingredients.sql", false)